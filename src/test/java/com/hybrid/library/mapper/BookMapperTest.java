package com.hybrid.library.mapper;

import com.hybrid.library.dto.BookDTO;
import com.hybrid.library.entity.Book;
import com.hybrid.library.mapper.impl.BookMapperImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class BookMapperTest {

    @InjectMocks
    private BookMapperImpl bookMapper;

    @Test
    public void mapToEntity_bookDto_mappingDtoToEntity() {
        BookDTO bookDTO = BookDTO.Builder()
                            .id(1L)
                            .author("Fjodor Dostojevski")
                            .title("Idiot")
                            .isbn("978-1-86197-876-9")
                            .build();

        Book book = bookMapper.mapToEntity(bookDTO);

        assertThat(book.getId()).isEqualTo(Long.valueOf(1L));
        assertThat(book.getAuthor()).isEqualTo("Fjodor Dostojevski");
        assertThat(book.getTitle()).isEqualTo("Idiot");
        assertThat(book.getIsbn()).isEqualTo("978-1-86197-876-9");
    }

    @Test
    public void mapToDTO_book_mappingEntityToDTO() {
        Book book = Book.Builder()
                    .id(1L)
                    .author("Fjodor Dostojevski")
                    .title("Idiot")
                    .isbn("978-1-86197-876-9")
                    .build();

        BookDTO bookDTO = bookMapper.mapToDto(book);

        assertThat(bookDTO.getId()).isEqualTo(Long.valueOf(1L));
        assertThat(bookDTO.getAuthor()).isEqualTo("Fjodor Dostojevski");
        assertThat(bookDTO.getTitle()).isEqualTo("Idiot");
        assertThat(bookDTO.getIsbn()).isEqualTo("978-1-86197-876-9");
    }
}
