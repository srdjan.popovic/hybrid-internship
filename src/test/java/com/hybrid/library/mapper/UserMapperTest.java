package com.hybrid.library.mapper;

import com.hybrid.library.dto.UserDTO;
import com.hybrid.library.entity.User;
import com.hybrid.library.mapper.impl.UserMapperImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class UserMapperTest {

    @InjectMocks
    private UserMapperImpl userMapper;

    @Test
    public void mapToEntity_userDto_mappingDtoToEntity() {
        UserDTO userDTO = UserDTO.Builder()
                            .id(1L)
                            .firstName("Srdjan")
                            .lastName("Popovic")
                            .username("pop14")
                            .email("pop@gmail.com")
                            .password("hybrid-it")
                            .build();

        User user = userMapper.mapToEntity(userDTO);

        assertThat(user.getId()).isEqualTo(Long.valueOf(1L));
        assertThat(user.getFirstName()).isEqualTo("Srdjan");
        assertThat(user.getLastName()).isEqualTo("Popovic");
        assertThat(user.getUsername()).isEqualTo("pop14");
        assertThat(user.getEmail()).isEqualTo("pop@gmail.com");
        assertThat(user.getPassword()).isEqualTo("hybrid-it");
    }

    @Test
    public void mapToDTO_user_mappingEntityToDTO() {
        User user = User.Builder()
                .id(1L)
                .firstName("Srdjan")
                .lastName("Popovic")
                .username("pop14")
                .email("pop@gmail.com")
                .password("hybrid-it")
                .build();

        UserDTO userDTO = userMapper.mapToDto(user);

        assertThat(userDTO.getId()).isEqualTo(Long.valueOf(1L));
        assertThat(userDTO.getFirstName()).isEqualTo("Srdjan");
        assertThat(userDTO.getLastName()).isEqualTo("Popovic");
        assertThat(userDTO.getUsername()).isEqualTo("pop14");
        assertThat(userDTO.getEmail()).isEqualTo("pop@gmail.com");
        assertThat(userDTO.getPassword()).isEqualTo("hybrid-it");
    }
}
