package com.hybrid.library.service;

import com.hybrid.library.dto.BookDTO;
import com.hybrid.library.entity.Book;
import com.hybrid.library.exception.NotFoundEntityException;
import com.hybrid.library.mapper.impl.BookMapperImpl;
import com.hybrid.library.repository.BookRepository;
import com.hybrid.library.service.impl.BookServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class BookServiceTest {

    @InjectMocks
    private BookServiceImpl bookService;

    @Mock
    private BookRepository bookRepository;

    @Mock
    private BookMapperImpl bookMapper;

    @Test
    public void create_book_bookSavedOk() {
        BookDTO bookDTO = BookDTO.Builder().build();

        when(bookMapper.mapToEntity(bookDTO)).thenReturn(Book.Builder().build());
        Book book = bookMapper.mapToEntity(bookDTO);

        bookService.create(bookDTO);
        verify(bookRepository, atMostOnce()).save(book);
    }

    @Test
    public void getById_book_getBookByIdOk() {
        Book book = Book.Builder().id(1L).build();

        when(bookRepository.findById(eq(1L))).thenReturn(Optional.of(book));

        when(bookMapper.mapToDto(book)).thenReturn(BookDTO.Builder().id(1L).build());
        BookDTO foundBook = bookService.getById(1L);

        assertThat(book.getId()).isEqualTo(foundBook.getId());
    }

    @Test
    public void getById_book_getBookByIdNotFound() {
        when(bookRepository.findById(eq(1L))).thenThrow(new NotFoundEntityException());

        assertThatThrownBy(() -> bookService.getById(1L)).isInstanceOf(NotFoundEntityException.class);
    }

    @Test
    public void update_book_bookUpdatedOk() {
        BookDTO bookDTO = BookDTO.Builder().id(1L).build();

        when(bookRepository.existsById(eq(1L))).thenReturn(true);

        when(bookMapper.mapToEntity(bookDTO)).thenReturn(Book.Builder().build());
        Book book = bookMapper.mapToEntity(bookDTO);

        bookService.update(bookDTO.getId(), bookDTO);
        verify(bookRepository, atMostOnce()).save(book);
    }

    @Test
    public void update_book_bookUpdateNotFound() {
        BookDTO bookDTO = BookDTO.Builder().id(2L).build();

        when(bookRepository.existsById(eq(1L))).thenThrow(new NotFoundEntityException());

        assertThatThrownBy(() -> bookService.update(1L, bookDTO)).isInstanceOf(NotFoundEntityException.class);
    }

    @Test
    public void delete_book_deleteBookByIdOk() {
        Book book = Book.Builder().id(1L).build();

        bookService.delete(1L);
        verify(bookRepository).deleteById(book.getId());
    }
}
