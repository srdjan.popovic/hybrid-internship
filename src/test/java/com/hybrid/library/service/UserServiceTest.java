package com.hybrid.library.service;

import com.hybrid.library.dto.UserDTO;
import com.hybrid.library.entity.User;
import com.hybrid.library.exception.NotFoundEntityException;
import com.hybrid.library.mapper.impl.UserMapperImpl;
import com.hybrid.library.repository.UserRepository;
import com.hybrid.library.service.impl.UserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapperImpl userMapper;

    @Test
    public void create_user_userSavedOk() {
        UserDTO userDTO = UserDTO.Builder().build();

        when(userMapper.mapToEntity(userDTO)).thenReturn(User.Builder().build());
        User user = userMapper.mapToEntity(userDTO);

        userService.create(userDTO);
        verify(userRepository, atMostOnce()).save(user);
    }

    @Test
    public void getById_user_getUserByIdOk() {
        User user = User.Builder().id(1L).build();

        when(userRepository.findById(eq(1L))).thenReturn(Optional.of(user));

        when(userMapper.mapToDto(user)).thenReturn(UserDTO.Builder().id(1L).build());
        UserDTO foundUser = userService.getById(1L);

        assertThat(user.getId()).isEqualTo(foundUser.getId());
    }

    @Test
    public void getById_user_getUserByIdNotFound() {
        when(userRepository.findById(eq(1L))).thenThrow(new NotFoundEntityException());

        assertThatThrownBy(() -> userService.getById(1L)).isInstanceOf(NotFoundEntityException.class);
    }

    @Test
    public void update_user_userUpdatedOk() {
        UserDTO userDTO = UserDTO.Builder().id(1L).build();

        when(userRepository.existsById(eq(1L))).thenReturn(true);

        when(userMapper.mapToEntity(userDTO)).thenReturn(User.Builder().build());
        User user = userMapper.mapToEntity(userDTO);

        userService.update(userDTO.getId(), userDTO);
        verify(userRepository, atMostOnce()).save(user);
    }

    @Test
    public void update_user_userUpdatedNotFound() {
        UserDTO userDTO = UserDTO.Builder().id(2L).build();

        when(userRepository.existsById(eq(1L))).thenThrow(new NotFoundEntityException());

        assertThatThrownBy(() -> userService.update(1L, userDTO)).isInstanceOf(NotFoundEntityException.class);
    }

    @Test
    public void delete_user_deleteUserByIdOk() {
        User user = User.Builder().id(1L).build();

        userService.delete(1L);
        verify(userRepository).deleteById(user.getId());
    }
}
