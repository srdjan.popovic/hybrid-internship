package com.hybrid.library.controller;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BookControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getById_book_getOneBookByIdOk() throws Exception {
        JSONObject body = new JSONObject()
                .put("title", "Get Book")
                .put("author", "Getting Book")
                .put("isbn", "978-1-86197-876-9");

        mockMvc.perform(MockMvcRequestBuilders.post("/books").contentType(MediaType.APPLICATION_JSON).content(body.toString())).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isCreated());

        mockMvc.perform(MockMvcRequestBuilders.get("/books/1")).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }

    @Test
    public void getById_book_getOneBookByIdNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/books/1")).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
    }

    //TODO: correct getAllBooks test
    @Test
    public void getAllBooks_book_getAllBooksOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/books")).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isEmpty());
    }

    @Test
    public void updateBook_book_updatingBookOk() throws Exception {
        JSONObject body = new JSONObject()
                .put("id", 1L)
                .put("title", "Book")
                .put("author", "Book Author")
                .put("isbn", "978-1-86197-876-9");

        mockMvc.perform(MockMvcRequestBuilders.post("/books").contentType(MediaType.APPLICATION_JSON).content(body.toString())).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isCreated());

        JSONObject updateBody = new JSONObject()
                .put("id", 1L)
                .put("title", "Updating Book")
                .put("author", "Updated Author")
                .put("isbn", "978-1-86197-876-9");

        mockMvc.perform(MockMvcRequestBuilders.put("/books/1").contentType(MediaType.APPLICATION_JSON).content(updateBody.toString())).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("Updating Book"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.author").value("Updated Author"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.isbn").value("978-1-86197-876-9"));
    }

    @Test
    public void updateBook_book_updatingBookNotFound() throws Exception {
        JSONObject updateBody = new JSONObject()
                .put("id", 1L)
                .put("title", "Updating Book")
                .put("author", "Updated Author")
                .put("isbn", "978-1-86197-876-9");

        mockMvc.perform(MockMvcRequestBuilders.put("/books/1").contentType(MediaType.APPLICATION_JSON).content(updateBody.toString())).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void deleteBook_book_deletingBookOk() throws Exception {
        JSONObject body = new JSONObject()
                .put("title", "Deleting Book")
                .put("author", "Deleted book")
                .put("isbn", "978-1-86197-876-9");

        mockMvc.perform(MockMvcRequestBuilders.post("/books").contentType(MediaType.APPLICATION_JSON).content(body.toString())).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isCreated());

        mockMvc.perform(MockMvcRequestBuilders.delete("/books/1")).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
