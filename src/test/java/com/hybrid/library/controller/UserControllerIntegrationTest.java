package com.hybrid.library.controller;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getById_user_getOneUserByIdOk() throws Exception {
        JSONObject body = new JSONObject()
                .put("firstName", "User")
                .put("lastName", "Last Name")
                .put("username", "username")
                .put("email", "user@gmail.com")
                .put("password", "hybrid-it");

        mockMvc.perform(MockMvcRequestBuilders.post("/users").contentType(MediaType.APPLICATION_JSON).content(body.toString())).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isCreated());

        mockMvc.perform(MockMvcRequestBuilders.get("/users/1")).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }

    @Test
    public void getById_user_getOneUserByIdNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/users/1")).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
    }

    //TODO: correct getAllUsers test
    @Test
    public void getAllUsers_user_getAllUsersOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/users")).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isEmpty());
    }

    @Test
    public void updateUser_user_updatingUserOk() throws Exception {
        JSONObject body = new JSONObject()
                .put("id", 1L)
                .put("firstName", "User")
                .put("lastName", "Last Name")
                .put("username", "username")
                .put("email", "user@gmail.com")
                .put("password", "hybrid-it");

        mockMvc.perform(MockMvcRequestBuilders.post("/users").contentType(MediaType.APPLICATION_JSON).content(body.toString())).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isCreated());

        JSONObject updateBody = new JSONObject()
                .put("id", 1L)
                .put("firstName", "Updated user")
                .put("lastName", "Updated LastName")
                .put("username", "Updated username")
                .put("email", "user-updated@gmail.com")
                .put("password", "hybrid-it");

        mockMvc.perform(MockMvcRequestBuilders.put("/users/1").contentType(MediaType.APPLICATION_JSON).content(updateBody.toString())).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("Updated username"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.password").value("hybrid-it"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value("Updated user"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value("Updated LastName"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("user-updated@gmail.com"));

    }

    @Test
    public void updateUser_user_updatingUserNotFound() throws Exception {
        JSONObject body = new JSONObject()
                .put("id", 1L)
                .put("firstName", "User")
                .put("lastName", "Last Name")
                .put("username", "username")
                .put("email", "user@gmail.com")
                .put("password", "hybrid-it");

        mockMvc.perform(MockMvcRequestBuilders.put("/users/1").contentType(MediaType.APPLICATION_JSON).content(body.toString())).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void deleteUser_user_deletingUserOk() throws Exception {
        JSONObject body = new JSONObject()
                .put("id", 1L)
                .put("firstName", "User")
                .put("lastName", "Last Name")
                .put("username", "username")
                .put("email", "user@gmail.com")
                .put("password", "hybrid-it");

        mockMvc.perform(MockMvcRequestBuilders.post("/users").contentType(MediaType.APPLICATION_JSON).content(body.toString())).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isCreated());

        mockMvc.perform(MockMvcRequestBuilders.delete("/users/1")).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
