package com.hybrid.library.controller;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BookCopyControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getById_bookCopy_getOneBookCopyByIdOk() throws Exception {
        JSONObject body = new JSONObject()
                .put("title", "Get Book")
                .put("author", "Getting Book")
                .put("isbn", "978-1-86197-876-9");

        mockMvc.perform(MockMvcRequestBuilders.post("/books").contentType(MediaType.APPLICATION_JSON).content(body.toString())).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isCreated());

        JSONObject bodyBookCopy = new JSONObject()
                .put("bookId", 1L)
                .put("hybridId", 3L);

        mockMvc.perform(MockMvcRequestBuilders.post("/book-copies").contentType(MediaType.APPLICATION_JSON).content(bodyBookCopy.toString())).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isCreated());

        mockMvc.perform(MockMvcRequestBuilders.get("/book-copies/2")).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(2));
    }

    @Test
    public void getById_bookCopy_getOneBookCopyByIdNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/book-copies/1")).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
    }

    //TODO: correct getAllBookCopies test
    @Test
    public void getAllBookCopies_bookCopy_getAllBookCopiesOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/book-copies")).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isEmpty());
    }

    @Test
    public void deleteBook_book_deletingBookOk() throws Exception {
        JSONObject body = new JSONObject()
                .put("title", "Book")
                .put("author", "Book")
                .put("isbn", "978-1-86197-876-9");

        mockMvc.perform(MockMvcRequestBuilders.post("/books").contentType(MediaType.APPLICATION_JSON).content(body.toString())).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isCreated());

        JSONObject bodyBookCopy = new JSONObject()
                .put("bookId", 1L)
                .put("hybridId", 3L);


        mockMvc.perform(MockMvcRequestBuilders.post("/book-copies").contentType(MediaType.APPLICATION_JSON).content(bodyBookCopy.toString())).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isCreated());

        mockMvc.perform(MockMvcRequestBuilders.delete("/book-copies/2")).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
