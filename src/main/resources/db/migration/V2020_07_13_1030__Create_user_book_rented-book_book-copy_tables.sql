CREATE TABLE book (
  id      SERIAL          PRIMARY KEY,
  title   VARCHAR         NOT NULL,
  author  VARCHAR         NOT NULL,
  isbn    VARCHAR(17)     UNIQUE NOT NULL,
  created_at TIMESTAMP    NOT NULL
);

CREATE TABLE "user" (
  id          SERIAL          PRIMARY KEY,
  first_name  VARCHAR         NOT NULL ,
  last_name   VARCHAR         NOT NULL ,
  username    VARCHAR         UNIQUE NOT NULL ,
  password    VARCHAR         NOT NULL,
  email       VARCHAR         UNIQUE NOT NULL
);

CREATE TABLE rented_book (
  id          SERIAL          PRIMARY KEY,
  user_id     INTEGER         REFERENCES "user"(id) NOT NULL,
  rented_date  TIMESTAMP      NOT NULL,
  return_date  TIMESTAMP
);

CREATE TABLE book_copy (
  id                SERIAL        PRIMARY KEY,
  hybrid_id         VARCHAR       NOT NULL,
  book_id           INTEGER       REFERENCES book(id) NOT NULL,
  rented_book_id    INTEGER       REFERENCES rented_book(id)
);
