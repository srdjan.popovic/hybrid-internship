package com.hybrid.library.exception;

public class BadIdRequestException extends RuntimeException {

    public BadIdRequestException() {
        super();
    }

    public BadIdRequestException(String message) {
        super(message);
    }
}
