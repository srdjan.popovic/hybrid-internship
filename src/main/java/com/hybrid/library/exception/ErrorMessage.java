package com.hybrid.library.exception;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ErrorMessage {

    private String message;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss.S")
    private LocalDateTime timestamp;

    private List<String> details = new ArrayList<>();

    public ErrorMessage(String message, LocalDateTime timestamp, List<String> details) {
        this.message = message;
        this.timestamp = timestamp;
        this.details = details;
    }

    public ErrorMessage() {

    }

    public static Builder Builder() {
        return new Builder();
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public List<String> getDetails() {
        return details;
    }

    public static class Builder {

        private String message;

        private LocalDateTime timestamp;

        private List<String> details = new ArrayList<>();

        private Builder() {

        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder timestamp(LocalDateTime timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder details(List<String> details) {
            this.details = details;
            return this;
        }

        public ErrorMessage build() {
            return new ErrorMessage(message, timestamp, details);
        }
    }
}
