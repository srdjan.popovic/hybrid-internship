package com.hybrid.library.exception;

public class OverdueBookException extends RuntimeException {

    public OverdueBookException() {
        super();
    }

    public OverdueBookException(String message) {
        super(message);
    }
}
