package com.hybrid.library.exception;

public class MaximumAmountRentedException extends RuntimeException {

    public MaximumAmountRentedException() {
        super();
    }

    public MaximumAmountRentedException(String message) {
        super(message);
    }
}
