package com.hybrid.library.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Set;

public class BookDTO extends AbstractBaseDTO {

    @NotBlank(message = "Book title must not be null")
    @Size(message = "Size cannot be less than 2, and more than 255 characters", min = 2, max = 255)
    private String title;

    @NotBlank(message = "Book author must not be null")
    @Pattern(regexp = "[A-Za-z]+[\\s]*[A-Za-z]*")
    private String author;

    @NotBlank(message = "Book ISBN must not be null")
    @Pattern(message = "ISBN must be in correct format (example: 978-1-86197-876-9)", regexp = "97[89]-[0-9]-[0-9]{5}-[0-9]{3}-9$")
    private String isbn;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private LocalDateTime createdAt;

    private Set<BookCopyDTO> bookCopies;

    public BookDTO(Long id, String title, String author, String isbn, LocalDateTime createdAt, Set<BookCopyDTO> bookCopies) {
        super(id);
        this.title = title;
        this.author = author;
        this.isbn = isbn;
        this.createdAt = createdAt;
        this.bookCopies = bookCopies;
    }

    public BookDTO() {

    }

    public static Builder Builder() {
        return new Builder();
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getIsbn() {
        return isbn;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public Set<BookCopyDTO> getBookCopies() {
        return bookCopies;
    }

    public static class Builder {

        private Long id;

        private String title;

        private String author;

        private String isbn;

        private LocalDateTime createdAt;

        private Set<BookCopyDTO> bookCopies;

        private Builder() {

        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder isbn(String isbn) {
            this.isbn = isbn;
            return this;
        }

        public Builder createdAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public Builder bookCopies(Set<BookCopyDTO> bookCopies) {
            this.bookCopies = bookCopies;
            return this;
        }

        public BookDTO build() {
            return new BookDTO(id, title, author, isbn, createdAt, bookCopies);
        }
    }
}
