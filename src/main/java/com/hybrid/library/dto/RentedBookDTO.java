package com.hybrid.library.dto;

import java.time.LocalDateTime;
import java.util.Set;

public class RentedBookDTO extends AbstractBaseDTO {

    private Long userId;

    private Set<BookCopyDTO> bookCopies;

    private LocalDateTime rentedDate;

    private LocalDateTime returnDate;

    public RentedBookDTO(Long id, Long userId, Set<BookCopyDTO> bookCopies, LocalDateTime rentedDate, LocalDateTime returnDate) {
        super(id);
        this.userId = userId;
        this.bookCopies = bookCopies;
        this.rentedDate = rentedDate;
        this.returnDate = returnDate;
    }

    public RentedBookDTO() {

    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDateTime getRentedDate() {
        return rentedDate;
    }

    public void setRentedDate(LocalDateTime rentedDate) {
        this.rentedDate = rentedDate;
    }

    public LocalDateTime getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDateTime returnDate) {
        this.returnDate = returnDate;
    }

    public Set<BookCopyDTO> getBookCopies() {
        return bookCopies;
    }

    public void setBookCopies(Set<BookCopyDTO> bookCopies) {
        this.bookCopies = bookCopies;
    }
}
