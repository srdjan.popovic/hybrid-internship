package com.hybrid.library.dto;

import java.time.LocalDate;

public class BookCopyDTO extends AbstractBaseDTO {

    private Long bookId;

    private Long hybridId;

    private Long rentedBookId;

    public BookCopyDTO(Long id, Long bookId, Long hybridId, Long rentedBookId) {
        super(id);
        this.bookId = bookId;
        this.hybridId = hybridId;
        this.rentedBookId = rentedBookId;
    }

    public BookCopyDTO() {

    }

    public static Builder Builder() {
        return new Builder();
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Long getHybridId() {
        return hybridId;
    }

    public void setHybridId(Long hybridId) {
        this.hybridId = hybridId;
    }

    public Long getRentedBookId() {
        return rentedBookId;
    }

    public void setRentedBookId(Long rentedBookId) {
        this.rentedBookId = rentedBookId;
    }

    public static class Builder {

        private Long id;

        private Long bookId;

        private Long hybridId;

        private Long rentedBookId;

        public Builder(Long id, Long bookId, Long hybridId, Boolean rented, LocalDate dateRented, Long userId, Long rentedBookId) {
            this.id = id;
            this.bookId = bookId;
            this.hybridId = hybridId;
            this.rentedBookId = rentedBookId;
        }

        private Builder() {

        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder bookId(Long bookId) {
            this.bookId = bookId;
            return this;
        }

        public Builder hybridId(Long hybridId) {
            this.hybridId = hybridId;
            return this;
        }

        public Builder rentedBookId(Long rentedBookId) {
            this.rentedBookId = rentedBookId;
            return this;
        }

        public BookCopyDTO build() {
            return new BookCopyDTO(id, bookId, hybridId, rentedBookId);
        }
    }
}
