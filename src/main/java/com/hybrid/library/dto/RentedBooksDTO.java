package com.hybrid.library.dto;

import javax.validation.constraints.Size;
import java.util.Set;

public class RentedBooksDTO {

    @Size(min = 1, max = 3)
    private Set<Long> booksId;

    public RentedBooksDTO(Set<Long> booksId) {
        this.booksId = booksId;
    }

    public RentedBooksDTO() {

    }

    public Set<Long> getBooksId() {
        return booksId;
    }

    public void setBooksId(Set<Long> booksId) {
        this.booksId = booksId;
    }
}
