package com.hybrid.library.dto;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractBaseDTO implements DTO {

    private Long id;

    public AbstractBaseDTO(Long id) {
        this.id = id;
    }

    public AbstractBaseDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
