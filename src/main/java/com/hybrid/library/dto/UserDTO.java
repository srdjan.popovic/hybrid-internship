package com.hybrid.library.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class UserDTO extends AbstractBaseDTO {

    @NotBlank(message = "First name must not be null")
    @Pattern(regexp = "[A-Za-z]+[\\s]*[A-Za-z]*")
    private String firstName;

    @NotBlank(message = "Last name must not be null")
    @Pattern(regexp = "[A-Za-z]+[\\s]*[A-Za-z]*")
    private String lastName;

    @NotBlank(message = "Username must not be null")
    private String username;

    @NotBlank(message = "Email must not be null")
    @Email
    private String email;

    private String password;

    public UserDTO(Long id, String firstName, String lastName, String username, String email, String password) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public UserDTO() {

    }

    public static Builder Builder() {
        return new Builder();
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public static class Builder {

        private Long id;

        private String firstName;

        private String lastName;

        private String username;

        private String email;

        private String password;

        private Builder() {

        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder username(String username) {
            this.username = username;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public UserDTO build() {
            return new UserDTO(id, firstName, lastName, username, email, password);
        }
    }
}
