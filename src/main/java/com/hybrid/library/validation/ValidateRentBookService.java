package com.hybrid.library.validation;

import com.hybrid.library.dto.RentedBooksDTO;
import com.hybrid.library.entity.AbstractBaseEntity;
import com.hybrid.library.entity.RentedBook;
import com.hybrid.library.exception.BadIdRequestException;
import com.hybrid.library.exception.MaximumAmountRentedException;
import com.hybrid.library.exception.OverdueBookException;
import com.hybrid.library.mapper.BookMapper;
import com.hybrid.library.repository.BookCopyRepository;
import com.hybrid.library.repository.BookRepository;
import com.hybrid.library.repository.RentedBookRepository;
import com.hybrid.library.service.BookService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ValidateRentBookService {

    private final int maxBooksRented;

    private final int rentTime;

    private final ChronoUnit unit;

    private final RentedBookRepository rentedBookRepository;

    private final BookService bookService;

    private final BookRepository bookRepository;

    private final BookMapper bookMapper;

    public ValidateRentBookService(@Value("${rent-book.max-books-rented}") int maxBooksRented, BookCopyRepository bookCopyRepository, @Value("${rent-book.rentTimeValue}") int rentTime, @Value("${rent-book.rentTimeUnit}") String unit, RentedBookRepository rentedBookRepository, BookService bookService, BookRepository bookRepository, BookMapper bookMapper) {
        this.maxBooksRented = maxBooksRented;
        this.rentTime = rentTime;
        this.unit = ChronoUnit.valueOf(unit.toUpperCase());
        this.rentedBookRepository = rentedBookRepository;
        this.bookService = bookService;
        this.bookRepository = bookRepository;
        this.bookMapper = bookMapper;
    }

    private Boolean overdueCheck(RentedBook rentedBook, int value, TemporalUnit unit) {
        return rentedBook.getRentedDate().isBefore(rentedBook.getRentedDate().plus(value, unit));
    }

    private Set<Long> getAllExistingBooksIds(Set<Long> bookIds) {
        return bookRepository.findAllByIdIn(bookIds)
                .stream()
                .map(AbstractBaseEntity::getId)
                .collect(Collectors.toSet());

    }

    private List<RentedBook> getAllOverdueBooks(Long userId) {
        return rentedBookRepository.findAllByUser_Id(userId)
                .stream()
                .filter(book -> overdueCheck(book, rentTime, unit))
                .collect(Collectors.toList());
    }

    private Long getAllRentedBooksSizeByUserId(Long userId) {
        return rentedBookRepository.findAllByUser_Id(userId)
                .stream()
                .mapToLong(rentedBook -> rentedBook.getBookCopies().size())
                .sum();
    }

    public void validateIfUserCanRentBook(Long userId, RentedBooksDTO rentedBooksDTO) {
        Set<Long> existingIds = getAllExistingBooksIds(rentedBooksDTO.getBooksId());

        if (existingIds.size() != rentedBooksDTO.getBooksId().size()) {
            rentedBooksDTO.getBooksId().removeAll(existingIds);

            throw new BadIdRequestException("Book with id: " + rentedBooksDTO.getBooksId() + " does not exist in database.");
        }

        if (getAllRentedBooksSizeByUserId(userId) + rentedBooksDTO.getBooksId().size() > maxBooksRented) {
            throw new MaximumAmountRentedException(String.format("User with id: %d has already rented out maximum amount of books (3).", userId));
        }

        if (isNotEmpty(getAllOverdueBooks(userId))) {
            throw new OverdueBookException(String.format("User with id: %d has book that he hasn't returned on time.", userId));
        }
    }

    private boolean isNotEmpty(List<RentedBook> list) {
        return !list.isEmpty();
    }
}
