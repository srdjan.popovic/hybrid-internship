package com.hybrid.library.controller;

import com.hybrid.library.dto.BookCopyDTO;
import com.hybrid.library.dto.RentedBooksDTO;
import com.hybrid.library.service.RentedBookService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@RestController
@RequestMapping("/rented-books")
public class RentedBookController {

    private final RentedBookService rentedBookService;

    public RentedBookController(RentedBookService rentedBookService) {
        this.rentedBookService = rentedBookService;
    }

    @PostMapping("/user/{id}")
    public Set<BookCopyDTO> rentBooks(@RequestBody @Valid RentedBooksDTO rentedBooksDTO, @PathVariable Long id) {
        return rentedBookService.rentBooks(rentedBooksDTO, id);
    }
}
