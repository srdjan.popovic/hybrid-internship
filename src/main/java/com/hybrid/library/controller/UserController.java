package com.hybrid.library.controller;

import com.hybrid.library.dto.UserDTO;
import com.hybrid.library.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Operation(summary = "Get all users from database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Users retrieved from database",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDTO.class))})
    })
    @GetMapping
    private ResponseEntity<Page<UserDTO>> getAll(Pageable pageable) {
        return ResponseEntity.ok(userService.getAll(pageable));
    }

    @Operation(summary = "Get user by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found user in database",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDTO.class))}),
            @ApiResponse(responseCode = "404", description = "User not found in database",
                    content = @Content)
    })
    @GetMapping("/{id}")
    private ResponseEntity<UserDTO> getById(@Parameter(description = "Id of a user to be found") @PathVariable Long id) {
        return ResponseEntity.ok(userService.getById(id));
    }

    @Operation(summary = "Create new user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successfully created new user",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request sent, not correct body passed",
                    content = @Content)
    })
    @PostMapping
    private ResponseEntity<UserDTO> create(@Parameter(description = "Passing object from which User is created") @Valid @RequestBody UserDTO userDTO) {
        return new ResponseEntity(userService.create(userDTO), HttpStatus.CREATED);
    }

    @Operation(summary = "Edit existing user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully edited existing user",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request sent, not correct body passed",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Invalid id supplied",
                    content = @Content)
    })
    @PutMapping("/{id}")
    private ResponseEntity<UserDTO> update(@Parameter(description = "Id of a user to be edited") @PathVariable Long id, @RequestBody UserDTO userDTO) {
        return ResponseEntity.ok(userService.update(id, userDTO));
    }

    @Operation(summary = "Delete existing user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Successfully deleted user",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDTO.class))}),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content)
    })
    @DeleteMapping("/{id}")
    private ResponseEntity<Void> delete(@Parameter(description = "Id of a user to be deleted") @PathVariable Long id) {
        userService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
