package com.hybrid.library.controller;

import com.hybrid.library.dto.BookCopyDTO;
import com.hybrid.library.service.BookCopyService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/book-copies")
public class BookCopyController {

    private final BookCopyService bookCopyService;

    public BookCopyController(BookCopyService bookCopyService) {
        this.bookCopyService = bookCopyService;
    }

    @Operation(summary = "Get all book copies from database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Book copies retrieved from database",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookCopyDTO.class))})
    })
    @GetMapping
    private ResponseEntity<Page<BookCopyDTO>> getAll(Pageable pageable) {
        return ResponseEntity.ok(bookCopyService.getAll(pageable));
    }

    @Operation(summary = "Get all book copies from database that belong to certain book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Book copies retrieved from database",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookCopyDTO.class))})
    })
    @GetMapping("/available/book/{id}")
    private ResponseEntity<List<BookCopyDTO>> getAllByBookAndNotRented(@Parameter(description = "Id of a book that book copy belongs to") @PathVariable Long id) {
        return ResponseEntity.ok(bookCopyService.getAllByBookAndNotRented(id));
    }

    @Operation(summary = "Get a book copy by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found book copy in database",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookCopyDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Book copy not found",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content)
    })
    @GetMapping("/{id}")
    private ResponseEntity<BookCopyDTO> getById(@Parameter(description = "Id of a book copy to be found") @PathVariable Long id) {
        return ResponseEntity.ok(bookCopyService.getById(id));
    }

    @Operation(summary = "Create new book copy")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successfully created new book copy",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookCopyDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request sent, not correct body passed",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Book copy not found",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content)
    })
    @PostMapping
    private ResponseEntity<BookCopyDTO> create(@Parameter(description = "Passing object from which BookCopy is created") @Valid @RequestBody BookCopyDTO bookCopyDTO) {
        return new ResponseEntity(bookCopyService.create(bookCopyDTO), HttpStatus.CREATED);
    }

    @Operation(summary = "Edit existing book copy")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully edited existing book copy",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookCopyDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request sent, not correct body passed",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Book copy not found",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content)
    })
    @PutMapping("/{id}")
    private ResponseEntity<BookCopyDTO> update(@Parameter(description = "Id of a book copy to be edited") @PathVariable Long id, @RequestBody BookCopyDTO bookCopyDTO) {
        return ResponseEntity.ok(bookCopyService.update(id, bookCopyDTO));
    }

    @Operation(summary = "Delete existing book copy")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Successfully deleted book copy",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookCopyDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Book copy not found",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content)
    })
    @DeleteMapping("/{id}")
    private ResponseEntity<Void> delete(@Parameter(description = "Id of a book copy to be deleted") @PathVariable Long id) {
        bookCopyService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
