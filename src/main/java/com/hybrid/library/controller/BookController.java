package com.hybrid.library.controller;

import com.hybrid.library.dto.BookDTO;
import com.hybrid.library.service.BookService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @Operation(summary = "Get all books from database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Books retrieved from database",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookDTO.class))})
    })
    @GetMapping
    private ResponseEntity<Page<BookDTO>> getAll(Pageable pageable, @RequestParam(required = false) String isbn) {
        return ResponseEntity.ok(Optional.ofNullable(isbn)
                .map(book -> bookService.getAllByIsbn(isbn, pageable))
                .orElseGet(() -> bookService.getAll(pageable)));
    }

    @Operation(summary = "Get a book by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found book in database",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Book not found in database",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookDTO.class))})
    })
    @GetMapping("/{id}")
    private ResponseEntity<BookDTO> getById(@Parameter(description = "Id of a book to be found") @PathVariable Long id) {
        return ResponseEntity.ok(bookService.getById(id));
    }

    @Operation(summary = "Create new book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successfully created new book",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request sent, not correct body passed",
                    content = @Content)
    })
    @PostMapping
    private ResponseEntity<BookDTO> create(@Parameter(description = "Passing object from which Book is created") @Valid @RequestBody BookDTO bookDTO) {
        return new ResponseEntity(bookService.create(bookDTO), HttpStatus.CREATED);
    }

    @Operation(summary = "Edit existing book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully edited existing book",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request sent, not correct body passed",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Book not found",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content)
    })
    @PutMapping("/{id}")
    private ResponseEntity<BookDTO> update(@Parameter(description = "Id of a book to be edited") @PathVariable Long id, @RequestBody BookDTO bookDTO) {
        return ResponseEntity.ok(bookService.update(id, bookDTO));
    }

    @Operation(summary = "Delete existing book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Successfully deleted book",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = BookDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Book not found",
                    content = @Content)
    })
    @DeleteMapping("/{id}")
    private ResponseEntity<Void> delete(@Parameter(description = "Id of a book to be deleted") @PathVariable Long id) {
        bookService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
