package com.hybrid.library.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

@Aspect
@Component
public class TimeLogging {

    private static Logger LOGGER = LoggerFactory.getLogger(TimeLogging.class);

    @Around("@annotation(com.hybrid.library.aspect.Trace)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {

        final LocalTime start = LocalTime.now();

        LOGGER.info("Invoked method {} with arguments: {}", joinPoint.getSignature(), joinPoint.getArgs());
        LOGGER.debug("Method {} started execution at: {}", joinPoint.getSignature(), start);

        Object proceed = joinPoint.proceed();

        final LocalTime end = LocalTime.now();

        LOGGER.debug("Method {} finished with execution at: {}", joinPoint.getSignature(), end);


        final long executionTime = ChronoUnit.MILLIS.between(start, end);

        LOGGER.debug("Time needed for method {} to finish was: {} milliseconds", joinPoint.getSignature(), executionTime);

        return proceed;
    }

}
