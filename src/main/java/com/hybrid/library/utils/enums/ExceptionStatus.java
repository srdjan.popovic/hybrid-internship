package com.hybrid.library.utils.enums;

public enum ExceptionStatus {
    NOT_FOUND,
    INTERNAL_SERVER_ERROR
}
