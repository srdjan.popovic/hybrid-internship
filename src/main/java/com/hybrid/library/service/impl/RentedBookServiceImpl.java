package com.hybrid.library.service.impl;

import com.hybrid.library.dto.BookCopyDTO;
import com.hybrid.library.dto.RentedBooksDTO;
import com.hybrid.library.entity.BookCopy;
import com.hybrid.library.entity.RentedBook;
import com.hybrid.library.exception.NotFoundEntityException;
import com.hybrid.library.mapper.BookCopyMapper;
import com.hybrid.library.repository.BookCopyRepository;
import com.hybrid.library.repository.RentedBookRepository;
import com.hybrid.library.repository.UserRepository;
import com.hybrid.library.service.BookService;
import com.hybrid.library.service.RentedBookService;
import com.hybrid.library.validation.ValidateRentBookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RentedBookServiceImpl implements RentedBookService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RentedBookServiceImpl.class);

    private final RentedBookRepository rentedBookRepository;

    private final BookCopyRepository bookCopyRepository;

    private final UserRepository userRepository;

    private final ValidateRentBookService validateRentBookService;

    private final BookService bookService;

    private final BookCopyMapper bookCopyMapper;

    public RentedBookServiceImpl(RentedBookRepository rentedBookRepository, BookCopyRepository bookCopyRepository, UserRepository userRepository, ValidateRentBookService validateRentBookService, BookService bookService, BookCopyMapper bookCopyMapper) {
        this.rentedBookRepository = rentedBookRepository;
        this.bookCopyRepository = bookCopyRepository;
        this.userRepository = userRepository;
        this.validateRentBookService = validateRentBookService;
        this.bookService = bookService;
        this.bookCopyMapper = bookCopyMapper;
    }

    @Override
    public Set<BookCopyDTO> rentBooks(RentedBooksDTO booksId, Long userId) {

        validateRentBookService.validateIfUserCanRentBook(userId, booksId);
        RentedBook rentedBook = new RentedBook();

        rentedBook.setUser(userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundEntityException(String.format("User with id: %d doesn't exist.", userId))));
        rentedBook.setRentedDate(LocalDateTime.now());

        Set<BookCopy> books = bookService.getAllAvailableBookCopiesByBookIds(booksId.getBooksId())
                .stream()
                .map(bookCopyMapper::mapToEntity)
                .collect(Collectors.toSet());

        books.forEach(bookCopy -> bookCopy.setRentedBook(rentedBook));

        rentedBookRepository.save(rentedBook);

        bookCopyRepository.saveAll(books);

        return books.stream()
                .map(bookCopyMapper::mapToDto)
                .collect(Collectors.toSet());
    }
}
