package com.hybrid.library.service.impl;

import com.hybrid.library.dto.UserDTO;
import com.hybrid.library.entity.User;
import com.hybrid.library.exception.NotFoundEntityException;
import com.hybrid.library.mapper.UserMapper;
import com.hybrid.library.repository.UserRepository;
import com.hybrid.library.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public UserDTO create(UserDTO userDTO) {
        LOGGER.debug("Creating new user with first name: {}, last name: {}, username: {}, email: {}.",
                userDTO.getFirstName(), userDTO.getLastName(), userDTO.getUsername(), userDTO.getEmail());
        User user = userMapper.mapToEntity(userDTO);

        userRepository.save(user);
        LOGGER.info("New user with id: {} is created.", user.getId());

        return userMapper.mapToDto(user);
    }

    @Override
    public UserDTO update(Long id, UserDTO userDTO) {
        if (userRepository.existsById(id) && id.equals(userDTO.getId())) {
            User user = userRepository.save(userMapper.mapToEntity(userDTO));
            LOGGER.info("User with id: {} is updated.", id);
            return userMapper.mapToDto(user);
        } else {
            throw new NotFoundEntityException(String.format("User with id: %d doesn't exist.", id));
        }
    }

    @Transactional
    @Override
    public void delete(Long id) {
        userRepository.deleteById(id);
        LOGGER.info("User with id: {} is deleted.", id);
    }

    @Override
    public Page<UserDTO> getAll(Pageable pageable) {
        LOGGER.debug("Getting all users.");
        return userRepository.findAll(pageable)
                .map(userMapper::mapToDto);
    }

    @Override
    public UserDTO getById(Long id) {
        LOGGER.debug("Getting user with id: {}.", id);
        return userMapper.mapToDto(userRepository.findById(id)
                .orElseThrow(() -> new NotFoundEntityException(String.format("User with id: %d doesn't exist", id))));
    }
}
