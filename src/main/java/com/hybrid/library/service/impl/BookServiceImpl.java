package com.hybrid.library.service.impl;

import com.hybrid.library.aspect.Trace;
import com.hybrid.library.dto.BookCopyDTO;
import com.hybrid.library.dto.BookDTO;
import com.hybrid.library.entity.Book;
import com.hybrid.library.exception.NotFoundEntityException;
import com.hybrid.library.mapper.BookMapper;
import com.hybrid.library.repository.BookRepository;
import com.hybrid.library.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
public class BookServiceImpl implements BookService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookServiceImpl.class);

    private final BookRepository bookRepository;

    private final BookMapper bookMapper;

    public BookServiceImpl(BookRepository bookRepository, BookMapper bookMapper) {
        this.bookRepository = bookRepository;
        this.bookMapper = bookMapper;
    }

    @Trace
    @Override
    public BookDTO create(BookDTO bookDTO) {
        LOGGER.debug("Creating new book with title: {}, author: {}, isbn: {}.",
                bookDTO.getTitle(), bookDTO.getAuthor(), bookDTO.getIsbn());
        Book book = bookMapper.mapToEntity(bookDTO);

        bookRepository.save(book);
        LOGGER.info("New book with id: {} is created.", book.getId());

        return bookMapper.mapToDto(book);
    }

    @Trace
    @Override
    public BookDTO update(Long id, BookDTO bookDTO) {
        if (bookRepository.existsById(id) && id.equals(bookDTO.getId())) {
            Book book = bookRepository.save(bookMapper.mapToEntity(bookDTO));
            LOGGER.info("Book with id: {} is updated.", id);
            return bookMapper.mapToDto(book);
        } else {
            throw new NotFoundEntityException(String.format("Book with id: %d doesn't exist.", id));
        }
    }

    @Trace
    @Transactional
    @Override
    public void delete(Long id) {
        bookRepository.deleteById(id);
        LOGGER.info("Book with id: {} is deleted.", id);
    }

    @Trace
    @Override
    public Page<BookDTO> getAll(Pageable pageable) {
        LOGGER.debug("Getting all books.");
        return bookRepository.findAll(pageable)
                .map(bookMapper::mapToDto);
    }

    @Trace
    @Override
    public BookDTO getById(Long id) {
        LOGGER.debug("Getting book with id: {}.", id);
        return bookMapper.mapToDto(bookRepository.findById(id)
                .orElseThrow(() -> new NotFoundEntityException(String.format("Book with id: %d doesn't exist", id))));
    }

    @Override
    public Page<BookDTO> getAllByIsbn(String isbn, Pageable pageable) {
        LOGGER.debug("Getting all books with isbn: {}", isbn);
        return bookRepository.findAllByIsbnContainsIgnoreCase(isbn, pageable)
                .map(bookMapper::mapToDto);
    }


    @Override
    public Set<BookCopyDTO> getAllAvailableBookCopiesByBookIds(Set<Long> booksId) {
        Set<BookCopyDTO> bookCopies = new HashSet<>();

        for (Long id : booksId) {
            BookCopyDTO bookCopyAvailable = getById(id).getBookCopies()
                    .stream()
                    .filter(bookCopy -> bookCopy.getRentedBookId() == null)
                    .findAny()
                    .orElseThrow(() -> new NotFoundEntityException(String.format("Book with id: %d has no available copies for rent.", id)));

            bookCopies.add(bookCopyAvailable);
        }

        return bookCopies;
    }
}
