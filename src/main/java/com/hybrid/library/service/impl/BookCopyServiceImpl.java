package com.hybrid.library.service.impl;

import com.hybrid.library.dto.BookCopyDTO;
import com.hybrid.library.entity.BookCopy;
import com.hybrid.library.exception.NotFoundEntityException;
import com.hybrid.library.mapper.BookCopyMapper;
import com.hybrid.library.repository.BookCopyRepository;
import com.hybrid.library.repository.BookRepository;
import com.hybrid.library.service.BookCopyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookCopyServiceImpl implements BookCopyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookCopyServiceImpl.class);

    private final BookCopyRepository bookCopyRepository;

    private final BookCopyMapper bookCopyMapper;

    private final BookRepository bookRepository;

    public BookCopyServiceImpl(BookCopyRepository bookCopyRepository, BookCopyMapper bookCopyMapper, BookRepository bookRepository) {
        this.bookCopyRepository = bookCopyRepository;
        this.bookCopyMapper = bookCopyMapper;
        this.bookRepository = bookRepository;
    }

    @Override
    public BookCopyDTO create(BookCopyDTO bookCopyDTO) {
        LOGGER.debug("Creating new book copy that is an instance of book with id: {}, with hybridId: {}.", bookCopyDTO.getBookId(), bookCopyDTO.getHybridId());
        BookCopy bookCopy = bookCopyMapper.mapToEntity(bookCopyDTO);

        return bookCopyMapper.mapToDto(bookCopyRepository.save(bookCopy));
    }

    @Override
    public BookCopyDTO update(Long id, BookCopyDTO bookCopyDTO) {
        if (bookCopyRepository.existsById(id) && id.equals(bookCopyDTO.getId())) {
            BookCopy bookCopy = bookCopyRepository.save(bookCopyMapper.mapToEntity(bookCopyDTO));
            LOGGER.info("Book copy with id: {} is updated.", id);
            return bookCopyMapper.mapToDto(bookCopy);
        } else {
            throw new NotFoundEntityException(String.format("Book copy with id: %d doesn't exist.", id));
        }
    }

    @Override
    public void delete(Long id) {

        if (bookCopyRepository.findById(id).isEmpty()) {
            bookCopyRepository.deleteById(id);
            LOGGER.info("Book copy with id: {} is deleted.", id);
        } else {
            LOGGER.error("You can't delete book copy with id: {} that is already rented.", id);
        }
    }

    @Override
    public Page<BookCopyDTO> getAll(Pageable pageable) {
        LOGGER.debug("Getting all book copies.");
        return bookCopyRepository.findAll(pageable)
                .map(bookCopyMapper::mapToDto);
    }

    @Override
    public BookCopyDTO getById(Long id) {
        LOGGER.debug("Getting book copy with id: {}.", id);
        return bookCopyMapper.mapToDto(bookCopyRepository.findById(id)
                .orElseThrow(() -> new NotFoundEntityException(String.format("Book copy with id: %d doesn't exist", id))));
    }

    @Override
    public List<BookCopyDTO> getAllByBookAndNotRented(Long id) {
        LOGGER.debug("Getting all book copies from book with id: {}.", id);
        return bookCopyRepository.findAllByBook_Id_AndRentedBook_IsNull(id)
                .stream()
                .map(bookCopyMapper::mapToDto)
                .collect(Collectors.toList());
    }
}
