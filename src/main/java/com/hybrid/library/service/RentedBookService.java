package com.hybrid.library.service;

import com.hybrid.library.dto.BookCopyDTO;
import com.hybrid.library.dto.RentedBooksDTO;

import java.util.Set;

public interface RentedBookService {

    Set<BookCopyDTO> rentBooks(RentedBooksDTO bookCopiesId, Long userId);
}
