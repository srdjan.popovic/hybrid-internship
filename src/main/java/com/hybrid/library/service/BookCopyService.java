package com.hybrid.library.service;

import com.hybrid.library.dto.BookCopyDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BookCopyService {

    BookCopyDTO create(BookCopyDTO bookCopyDTO);

    BookCopyDTO update(Long id, BookCopyDTO bookCopyDTO);

    void delete(Long id);

    Page<BookCopyDTO> getAll(Pageable pageable);

    BookCopyDTO getById(Long id);

    List<BookCopyDTO> getAllByBookAndNotRented(Long id);
}
