package com.hybrid.library.service;

import com.hybrid.library.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {

    UserDTO create(UserDTO userDTO);

    UserDTO update(Long id, UserDTO userDTO);

    void delete(Long id);

    Page<UserDTO> getAll(Pageable pageable);

    UserDTO getById(Long id);
}
