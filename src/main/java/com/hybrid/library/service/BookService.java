package com.hybrid.library.service;

import com.hybrid.library.dto.BookCopyDTO;
import com.hybrid.library.dto.BookDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Set;

public interface BookService {

    BookDTO create(BookDTO bookDTO);

    BookDTO update(Long id, BookDTO bookDTO);

    void delete(Long id);

    Page<BookDTO> getAll(Pageable pageable);

    BookDTO getById(Long id);

    Page<BookDTO> getAllByIsbn(String isbn, Pageable pageable);

    Set<BookCopyDTO> getAllAvailableBookCopiesByBookIds(Set<Long> booksId);
}
