package com.hybrid.library.mapper;

import com.hybrid.library.dto.DTO;
import com.hybrid.library.entity.Entity;

public interface Mapper<DtoType extends DTO, EntityType extends Entity> {

    DtoType mapToDto(EntityType entity);

    EntityType mapToEntity(DtoType dto);
}
