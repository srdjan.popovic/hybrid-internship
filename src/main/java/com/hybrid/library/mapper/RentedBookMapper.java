package com.hybrid.library.mapper;

import com.hybrid.library.dto.RentedBookDTO;
import com.hybrid.library.entity.RentedBook;

public interface RentedBookMapper extends Mapper<RentedBookDTO, RentedBook> {
}
