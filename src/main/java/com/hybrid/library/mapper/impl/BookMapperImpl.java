package com.hybrid.library.mapper.impl;

import com.hybrid.library.dto.BookDTO;
import com.hybrid.library.entity.Book;
import com.hybrid.library.mapper.BookCopyMapper;
import com.hybrid.library.mapper.BookMapper;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class BookMapperImpl implements BookMapper {

    private final BookCopyMapper bookCopyMapper;

    public BookMapperImpl(BookCopyMapper bookCopyMapper) {
        this.bookCopyMapper = bookCopyMapper;
    }

    @Override
    public Book mapToEntity(BookDTO bookDTO) {
        return Book.Builder()
                .id(bookDTO.getId())
                .author(bookDTO.getAuthor())
                .title(bookDTO.getTitle())
                .isbn(bookDTO.getIsbn())
                .bookCopies(bookDTO.getBookCopies()
                        .stream()
                        .map(bookCopyMapper::mapToEntity)
                        .collect(Collectors.toSet()))
                .build();
    }

    @Override
    public BookDTO mapToDto(Book book) {
        return BookDTO.Builder()
                .id(book.getId())
                .author(book.getAuthor())
                .title(book.getTitle())
                .isbn(book.getIsbn())
                .createdAt(book.getCreatedAt())
                .bookCopies(book.getBookCopies()
                        .stream()
                        .map(bookCopyMapper::mapToDto)
                        .collect(Collectors.toSet()))
                .build();
    }
}
