package com.hybrid.library.mapper.impl;

import com.hybrid.library.dto.UserDTO;
import com.hybrid.library.entity.User;
import com.hybrid.library.mapper.UserMapper;
import org.springframework.stereotype.Component;

@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public User mapToEntity(UserDTO userDTO) {
        return User.Builder()
                .id(userDTO.getId())
                .firstName(userDTO.getFirstName())
                .lastName(userDTO.getLastName())
                .username(userDTO.getUsername())
                .email(userDTO.getEmail())
                .password(userDTO.getPassword())
                .build();
    }

    @Override
    public UserDTO mapToDto(User user) {
        return UserDTO.Builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .username(user.getUsername())
                .email(user.getEmail())
                .password(user.getPassword())
                .build();
    }
}
