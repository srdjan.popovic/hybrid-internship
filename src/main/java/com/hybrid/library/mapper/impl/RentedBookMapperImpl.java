package com.hybrid.library.mapper.impl;

import com.hybrid.library.dto.RentedBookDTO;
import com.hybrid.library.entity.RentedBook;
import com.hybrid.library.exception.NotFoundEntityException;
import com.hybrid.library.mapper.RentedBookMapper;
import com.hybrid.library.repository.UserRepository;
import org.springframework.stereotype.Component;

@Component
public class RentedBookMapperImpl implements RentedBookMapper {

    private final UserRepository userRepository;

    public RentedBookMapperImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public RentedBook mapToEntity(RentedBookDTO rentedBookDTO) {
        RentedBook rentedBook = new RentedBook();

        rentedBook.setId(rentedBookDTO.getId());
        rentedBook.setUser(userRepository.findById(rentedBookDTO.getUserId())
                .orElseThrow(() -> new NotFoundEntityException(String.format("Book copy with id: %d doesn't exist", rentedBookDTO.getUserId()))));

        return rentedBook;
    }

    @Override
    public RentedBookDTO mapToDto(RentedBook rentedBook) {
        RentedBookDTO rentedBookDTO = new RentedBookDTO();

        rentedBookDTO.setId(rentedBook.getId());
        rentedBookDTO.setUserId(rentedBook.getUser().getId());
        rentedBookDTO.setRentedDate(rentedBook.getRentedDate());
        rentedBookDTO.setReturnDate(rentedBookDTO.getReturnDate());

        return rentedBookDTO;
    }


}
