package com.hybrid.library.mapper.impl;

import com.hybrid.library.dto.BookCopyDTO;
import com.hybrid.library.entity.BookCopy;
import com.hybrid.library.exception.NotFoundEntityException;
import com.hybrid.library.mapper.BookCopyMapper;
import com.hybrid.library.repository.BookRepository;
import org.springframework.stereotype.Component;

@Component
public class BookCopyMapperImpl implements BookCopyMapper {

    private final BookRepository bookRepository;

    public BookCopyMapperImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public BookCopy mapToEntity(BookCopyDTO bookCopyDTO) {
        BookCopy bookCopy = new BookCopy();

        bookCopy.setId(bookCopyDTO.getId());
        bookCopy.setHybridId(bookCopyDTO.getHybridId());
        bookCopy.setBook(bookRepository.findById(bookCopyDTO.getBookId())
                .orElseThrow(() -> new NotFoundEntityException(String.format("Book with id: %d is not found.", bookCopyDTO.getBookId()))));

        return bookCopy;
    }

    @Override
    public BookCopyDTO mapToDto(BookCopy bookCopy) {
        BookCopyDTO bookCopyDTO = new BookCopyDTO();

        bookCopyDTO.setId(bookCopy.getId());
        bookCopyDTO.setHybridId(bookCopy.getHybridId());
        bookCopyDTO.setBookId(bookCopy.getBook().getId());
        bookCopyDTO.setRentedBookId(bookCopy.getRentedBook() == null ? null : bookCopy.getRentedBook().getId());

        return bookCopyDTO;
    }
}
