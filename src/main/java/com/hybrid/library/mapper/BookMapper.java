package com.hybrid.library.mapper;

import com.hybrid.library.dto.BookDTO;
import com.hybrid.library.entity.Book;

public interface BookMapper extends Mapper<BookDTO, Book> {
}
