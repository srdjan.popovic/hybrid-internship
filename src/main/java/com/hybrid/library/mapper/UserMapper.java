package com.hybrid.library.mapper;

import com.hybrid.library.dto.UserDTO;
import com.hybrid.library.entity.User;

public interface UserMapper extends Mapper<UserDTO, User> {
}
