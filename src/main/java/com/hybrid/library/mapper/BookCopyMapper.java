package com.hybrid.library.mapper;

import com.hybrid.library.dto.BookCopyDTO;
import com.hybrid.library.entity.BookCopy;

public interface BookCopyMapper extends Mapper<BookCopyDTO, BookCopy> {
}
