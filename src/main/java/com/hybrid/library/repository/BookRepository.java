package com.hybrid.library.repository;

import com.hybrid.library.entity.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface BookRepository extends JpaRepository<Book, Long> {

    Page<Book> findAllByIsbnContainsIgnoreCase(String isbn, Pageable pageable);

    Set<Book> findAllByIdIn(Set<Long> bookIds);
}
