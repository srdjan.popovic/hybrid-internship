package com.hybrid.library.repository;

import com.hybrid.library.entity.BookCopy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BookCopyRepository extends JpaRepository<BookCopy, Long> {

    List<BookCopy> findAllByBook_Id_AndRentedBook_IsNull(Long bookId);

    Optional<BookCopy> findByBook_Id_AndRentedBook_IsNull(Long bookId);
}
