package com.hybrid.library.repository;

import com.hybrid.library.entity.RentedBook;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RentedBookRepository extends JpaRepository<RentedBook, Long> {

    List<RentedBook> findAllByUser_Id(Long id);
}
