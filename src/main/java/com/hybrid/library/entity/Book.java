package com.hybrid.library.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
public class Book extends AbstractBaseEntity {

    private String title;

    private String author;

    @Column(unique = true)
    private String isbn;

    private LocalDateTime createdAt;

    @OneToMany(mappedBy = "book")
    private Set<BookCopy> bookCopies;

    public Book(Long id, String title, String author, String isbn, LocalDateTime createdAt, Set<BookCopy> bookCopies) {
        super(id);
        this.title = title;
        this.author = author;
        this.isbn = isbn;
        this.bookCopies = bookCopies;
        this.createdAt = LocalDateTime.now();
    }

    public Book() {

    }

    public static Builder Builder() {
        return new Builder();
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getIsbn() {
        return isbn;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public Set<BookCopy> getBookCopies() {
        return bookCopies;
    }

    public void setBookCopies(Set<BookCopy> bookCopies) {
        this.bookCopies = bookCopies;
    }

    public static class Builder {

        private Long id;

        private String title;

        private String author;

        private String isbn;

        private LocalDateTime createdAt;

        private Set<BookCopy> bookCopies;

        private Builder() {

        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder isbn(String isbn) {
            this.isbn = isbn;
            return this;
        }

        public Builder createdAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public Builder bookCopies(Set<BookCopy> bookCopies) {
            this.bookCopies = bookCopies;
            return this;
        }

        public Book build() {
            return new Book(id, title, author, isbn, createdAt, bookCopies);
        }
    }

}
