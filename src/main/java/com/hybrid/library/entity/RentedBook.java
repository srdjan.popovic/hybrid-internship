package com.hybrid.library.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
public class RentedBook extends AbstractBaseEntity {

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "rentedBook", cascade = CascadeType.MERGE)
    private Set<BookCopy> bookCopies;

    private LocalDateTime rentedDate;

    private LocalDateTime returnDate;

    public RentedBook(Long id, User user, Set<BookCopy> bookCopies, LocalDateTime rentedDate, LocalDateTime returnDate) {
        super(id);
        this.user = user;
        this.bookCopies = bookCopies;
        this.rentedDate = rentedDate;
        this.returnDate = returnDate;
    }

    public RentedBook() {

    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<BookCopy> getBookCopies() {
        return bookCopies;
    }

    public void setBookCopies(Set<BookCopy> bookCopies) {
        this.bookCopies = bookCopies;
    }

    public LocalDateTime getRentedDate() {
        return rentedDate;
    }

    public void setRentedDate(LocalDateTime rentedDate) {
        this.rentedDate = rentedDate;
    }

    public LocalDateTime getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDateTime returnDate) {
        this.returnDate = returnDate;
    }
}
