package com.hybrid.library.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class BookCopy extends AbstractBaseEntity {

    @ManyToOne
    private Book book;

    private Long hybridId;

    @ManyToOne
    private RentedBook rentedBook;

    public BookCopy(Long id, Book book, Long hybridId, RentedBook rentedBook) {
        super(id);
        this.book = book;
        this.hybridId = hybridId;
        this.rentedBook = rentedBook;
    }

    public BookCopy() {

    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Long getHybridId() {
        return hybridId;
    }

    public void setHybridId(Long hybridId) {
        this.hybridId = hybridId;
    }

    public RentedBook getRentedBook() {
        return rentedBook;
    }

    public void setRentedBook(RentedBook rentedBook) {
        this.rentedBook = rentedBook;
    }
}
